package no.experis.Task27.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "pizzas")
public class Pizza extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private boolean extraCheese;
    private boolean extraTomato;
    private boolean extraPepperoni;

    private int price;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "order_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Order order;

    public Long getId() {
        return id;
    }

    public int getPrice() {
        return this.price;
    }

    private void setPrice() {
        int newPrice = 100;
        if (extraCheese) newPrice += 30;
        if (extraTomato) newPrice += 20;
        if (extraPepperoni) newPrice += 40;
        this.price = newPrice;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isExtraCheese() {
        return extraCheese;
    }

    public void setExtraCheese(boolean extraCheese) {
        this.extraCheese = extraCheese;
        this.setPrice();
    }

    public boolean isExtraTomato() {
        return extraTomato;
    }

    public void setExtraTomato(boolean extraTomato) {
        this.extraTomato = extraTomato;
        this.setPrice();
    }

    public boolean isExtraPepperoni() {
        return extraPepperoni;
    }

    public void setExtraPepperoni(boolean extraPepperoni) {
        this.extraPepperoni = extraPepperoni;
        this.setPrice();
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
