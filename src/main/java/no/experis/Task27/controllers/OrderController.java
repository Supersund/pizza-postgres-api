package no.experis.Task27.controllers;


import no.experis.Task27.exceptions.ResourceNotFoundException;
import no.experis.Task27.models.Customer;
import no.experis.Task27.models.Order;
import no.experis.Task27.repositories.CustomerRepository;
import no.experis.Task27.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerRepository customerRepository;


    @GetMapping("/customers/{customerId}/orders")
    public ModelAndView getCustomers(@PathVariable Long customerId) {
        List<Order> orderList = orderRepository.findByCustomerId(customerId);
        String viewName = "orders";
        Map<String, Object> model = new HashMap<>();
        model.put("orders", orderList);
        model.put("customerId", customerId);
        return new ModelAndView(viewName, model);
    }

    @PostMapping("/customers/{customerId}/orders")
    public Order createOrder(@PathVariable Long customerId, @Valid @RequestBody Order order) {
        return customerRepository.findById(customerId).map(
                customer -> {
                    order.setCustomer(customer);
                    return orderRepository.save(order);
                }
        ).orElseThrow(() -> new ResourceNotFoundException("Customer not found with id " + customerId));
    }

    @DeleteMapping("customers/{customerId}/orders/{orderId}")
    public ResponseEntity<?> deleteOrder(@PathVariable Long orderId) {
        return orderRepository.findById(orderId).map(order -> {
            orderRepository.delete(order);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Order not found with id " + orderId));
    }
}
