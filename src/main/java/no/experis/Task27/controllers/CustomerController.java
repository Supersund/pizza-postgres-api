package no.experis.Task27.controllers;


import no.experis.Task27.exceptions.ResourceNotFoundException;
import no.experis.Task27.models.Customer;
import no.experis.Task27.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;


    @GetMapping(value = {"", "/", "/customers"})
    public ModelAndView getCustomers() {
        List<Customer> customerList = customerRepository.findAll();
        String viewName = "customers";
        Map<String, Object> model = new HashMap<>();
        model.put("customers", customerList);
        return new ModelAndView(viewName, model);
    }



    @PostMapping("/customers")
    public Customer createCustomer(@Valid @RequestBody Customer customer) {
        return customerRepository.save(customer);
    }

    @DeleteMapping("/customers/{customerId}")
    public ResponseEntity<?> deleteOrder(@PathVariable Long customerId) {
        return customerRepository.findById(customerId).map(customer -> {
            customerRepository.delete(customer);
            System.out.println("Was here");
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Customer not found with id " + customerId));
    }
}
