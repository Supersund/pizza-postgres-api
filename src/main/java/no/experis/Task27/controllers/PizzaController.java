package no.experis.Task27.controllers;


import no.experis.Task27.exceptions.ResourceNotFoundException;
import no.experis.Task27.models.Order;
import no.experis.Task27.models.Pizza;
import no.experis.Task27.repositories.OrderRepository;
import no.experis.Task27.repositories.PizzaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class PizzaController {

    @Autowired
    private PizzaRepository pizzaRepository;

    @Autowired
    private OrderRepository orderRepository;


    @GetMapping("/customers/{customerId}/orders/{orderId}/pizzas")
    public ModelAndView getPizzasByOrderId(@PathVariable Long customerId, @PathVariable Long orderId) {
        List<Pizza> pizzaList = pizzaRepository.findByOrderId(orderId);
        int price = 0;
        for (Pizza pizza: pizzaList) price+= pizza.getPrice();
        String viewname = "pizzas";
        Map<String, Object> model = new HashMap<>();
        model.put("pizzas", pizzaList);
        model.put("customerId", customerId);
        model.put("orderId", orderId);
        model.put("price", price);
        return new ModelAndView(viewname, model);
    }

    @PostMapping("/customers/{customerId}/orders/{orderId}/pizzas")
    public Pizza createPizza(@PathVariable Long customerId, @PathVariable Long orderId, @RequestBody Pizza pizza) {
        return orderRepository.findById(orderId).map(order -> {
            pizza.setOrder(order);
            return pizzaRepository.save(pizza);
        }).orElseThrow(() -> new ResourceNotFoundException("Order not found with id " + orderId));
    }

    @DeleteMapping("/customers/{customerId}/orders/{orderId}/pizzas/{pizzaId}")
    public ResponseEntity<?> deletePizza(@PathVariable Long pizzaId) {
        return pizzaRepository.findById(pizzaId).map(pizza -> {
            pizzaRepository.delete(pizza);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Pizza not found with id " + pizzaId));
    }
}
