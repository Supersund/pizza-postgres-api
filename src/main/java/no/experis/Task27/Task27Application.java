package no.experis.Task27;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class Task27Application {

	public static void main(String[] args) {
		SpringApplication.run(Task27Application.class, args);
	}

}
